# justifier-sips2019

This is the GitLab project for the `justifier` hackathon at SIPS2019.

The rendered version of the R Markdown file that contains the reproducible analyses for this project (using [the ROCK](https://rockbook.org)) is located at https://matherion.gitlab.io/justifier-sips2019.

Other relevant resources are:

- The 'official' SIPS2019 Google Doc landing page: https://docs.google.com/document/d/1kjSbgF97mFglEf6YZsyTwrNxYKOfBYOcDF7nqRkBWVY/edit
- The example `justifier` framework: https://docs.google.com/document/d/1MPZjUhA1LxeIg5NYGVK27tDuLusve4GJfDmcrTVvI7A/edit
- The session planning: https://docs.google.com/document/d/1aXr56gIxVltbMPWdLxUCRiPSp_VI1eSNV2AUbkktn3E/edit (ok, this one is hardly relevant, but might as well be complete about it)
- The `justifier` pkgdown site at https://r-packages.gitlab.io/justifier

